#!/bin/bash
if [ $# -eq 0 ]
  then
    echo "ERROR! Usage: help/video filepath"
    exit
fi

./darknet detector demo cfg/coco.data cfg/yolov3.cfg yolov3.weights $1 -prefix pictures
#ffmpeg -i pictures_%08d.jpg video.mp4
#ffmpeg -i video.mp4 -i $1 output.mp4
ffmpeg -i "pictures_%08d.jpg" -r 25 -c:v libx264 -crf 20  -pix_fmt yuv420p movie2.mp4

#rm pictures_*.jpg video.mp4

rm pictures_*.jpg
